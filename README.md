# Comandos GPG

Comandos principales para el uso de GPG en terminal. Guía completa de GPG en https://colectivodisonancia.net/herramientas/cifrado-con-gnupg-para-gnu-linux/



## Lista de comandos principales GPG

|  **Función**  | **Comando** |
| ---- | ---- |
| Cifrado simétrico sobre archivo  |   gpg -c [archivo]   |
| Descifar archivo   | gpg -d [archivo]     |
| Cifar simétrico con resultado en .asc     | gpg -ca [archivo]     |
| Cifrado Simétrico en AES256     |  gpg -c --cipher-algo aes256 [archivo]    |
| Generar par de llaves     | gpg --full-gen-key     |
| Ver lista de llaves     | gpg -k     |
| Exportar llave pública     | gpg --export -a [usuario] > [nombre].asc     |
| Exportar llave privada     | gpg --export-secret-key -a [user] > [nombre].asc     |
| Importar llave     | gpg --import [archivo de la llave]     |
| Cifrar con llave pública     | gpg -e [archivo]     |
| Cifrar con Ll. pública y firmar con Ll. Priv.     | gpg -u [llave privada usada] -se [archivo]     |
| Firmar por separado     | gpg -sb [archivo]     |
| Firmar solo texto | gpg -u [llave privada usada] --clearsign [archivo de texto] |
| Enviar llave a servidor     |  gpg --send-key [Llave]    |
| Buscar llave en servidor     |  gpg --search-key [Información de búsqueda]    |
| Firma local de llave pública     | gpg --lsign-key [llave a firmar]     |
| Firmar llave con Ll. privada específica | gpg - u [llave privada usada] --lsign-key [llave a firmar] |
| Firma de llave pública     |  gpg --sign-key [llave a firmar]    |
| Actualizar confianza de llaves     | gpg --update-trustdb     |
| Generar certificado de revocación     | gpg -ao [salida] --generate-revocation [Nombre]     |
| Eliminar llave pública     | gpg --delete-keys [llave]     |
| Eliminar llave privada     | gpg --delete-secret-keys [llave]     |
| Editar llave     | gpg --edit-key [llave]     |
| Consultar lista de comandos     | “gpg –help” y “man gpg”     |
| Editar gpg.config     | nano $HOME/.gnupg/gpg.conf     |

* De no poder accederse a los servidores desde el terminal, se debe instalar el paquete *dirmngr*

  ```bash
  sudo apt-get install dirmngr
  ```

  

## Configuración recomendada de gpg.config 

 

``` bash
keyserver hkps://hkps.pool.sks-keyservers.net 
cert-digest-algo SHA512 
no-emit-version 
no-comments 
personal-cipher-preferences AES256 AES192 AES CAST5 
personal-digest-preferences SHA512 SHA384 SHA256 SHA224
keyid-format 0xlong
with-fingerprint
```
